import { createStore, applyMiddleware  } from 'redux';
import thunk from 'redux-thunk';
import XHR from './xhr';

export function setResult(res) {
  return { type: 'SET_RESULT', payload: res };
}

export function getSuggestions(searchString) {
  return async function(dispatch, getState) {
    if (searchString.trim().length > 2) {
      await XHR.getSuggestions(searchString, (data) => {
        dispatch(setResult(data?.items));
      });
    } else {
      dispatch(setResult([]));
    }
  }
}

const initialState = {
  result: []
}

export function reducer(state = initialState, action) {

  switch(action.type) {

    case 'SET_RESULT': {
      return {
        ...state,
        result: action.payload
      }
    }
    default:
      return state;
  }
}

export const store = createStore(
  reducer,
  applyMiddleware(thunk)
);