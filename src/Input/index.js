import React, { useState, useRef, useEffect } from 'react';
import style from './input.module.css';

const CloseIcon = () => {
  return (
    <svg
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 330 330"
    >
      <path
        d="M165,0C120.926,0,79.492,17.163,48.328,48.327c-64.334,64.333-64.334,169.011-0.002,233.345
          C79.49,312.837,120.926,330,165,330c44.072,0,85.508-17.163,116.672-48.328c64.334-64.334,64.334-169.012,0-233.345
          C250.508,17.163,209.072,0,165,0z M239.246,239.245c-2.93,2.929-6.768,4.394-10.607,4.394c-3.838,0-7.678-1.465-10.605-4.394
          L165,186.213l-53.033,53.033c-2.93,2.929-6.768,4.394-10.607,4.394c-3.838,0-7.678-1.465-10.605-4.394
          c-5.859-5.857-5.859-15.355,0-21.213L143.787,165l-53.033-53.033c-5.859-5.857-5.859-15.355,0-21.213
          c5.857-5.857,15.355-5.857,21.213,0L165,143.787l53.031-53.033c5.857-5.857,15.355-5.857,21.213,0
          c5.859,5.857,5.859,15.355,0,21.213L186.213,165l53.033,53.032C245.104,223.89,245.104,233.388,239.246,239.245z"
      />
    </svg>
  )
}

const SearchIcon = () => {
  return (
    <svg
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 487.95 487.95"
    >
      <path
        d="M481.8,453l-140-140.1c27.6-33.1,44.2-75.4,44.2-121.6C386,85.9,299.5,0.2,193.1,0.2S0,86,0,191.4s86.5,191.1,192.9,191.1
          c45.2,0,86.8-15.5,119.8-41.4l140.5,140.5c8.2,8.2,20.4,8.2,28.6,0C490,473.4,490,461.2,481.8,453z M41,191.4
          c0-82.8,68.2-150.1,151.9-150.1s151.9,67.3,151.9,150.1s-68.2,150.1-151.9,150.1S41,274.1,41,191.4z"
      />
    </svg>
  )
}

export const Input = (props) => {
  const inputRef = useRef(null);
  const [showItems, setShowItems] = useState(false);

  const {
    items, value, width, disabled, id,
  } = props;

  const closeDropdown = () => {
    setShowItems(false);
  };

  const openDropdown = () => {
    setShowItems(true);
  };


  useEffect(() => {
    const handleDocumentClick = ({
      target,
    }) => {
      const { current } = inputRef;
  
      if (!current.contains(target)) {
        closeDropdown();
      }
    };

    document.addEventListener('click', handleDocumentClick);

    return () => {
      document.removeEventListener('click', handleDocumentClick);
    };
  }, []);

  useEffect(() => {
    if (items.length > 1 || (items.length === 1 && items[0] !== value)) {
      openDropdown();
    } else {
      closeDropdown();
    }
  }, [items, value]);

  const handleChange = (e) => {
    const { onChange } = props;
    const { value } = e.target;
    if (value.length === 0) {
      openDropdown();
    } else {
      closeDropdown();
    }
    onChange(value);
  };

  const handleSelectItem = (v) => {
    const { onChange } = props;
    onChange(v);
    closeDropdown();
  };

  const clearInput = () => {
    const { onChange, disabled } = props;
    onChange('');
    if (disabled) {
      openDropdown();
    }
  };

  const inputClickHandler = () => {
    if (showItems) {
      closeDropdown();
    } else {
      openDropdown();
    }
  };

  return (
    <div>
      <div
        style={{ width }}
        className={style['drop-container']}
      >
        <div
          className={style['drop-container__searchIcon']}
        >
          <SearchIcon />
        </div>
        <input
          id={id}
          ref={inputRef}
          type="text"
          disabled={disabled}
          value={value}
          onChange={handleChange}
          onClick={inputClickHandler}
          className={style['drop-container__input']}
          autoComplete="off"
          placeholder="Поиск по магазину"
        />
        <div className={showItems ? style['drop-container__list--open'] : style['drop-container__list--closed'] }>
          {showItems && items.map((el, i) => (
            <div
              key={i}
              onClick={() => handleSelectItem(el)}
              className={style['drop-container__item']}
            >
              {el}
            </div>
          ))}
        </div>
        <div
          onClick={clearInput}
          className={style['drop-container__closeBtn']}
        >
          <CloseIcon />
        </div>
      </div>
    </div>
  );
};

export default Input;
