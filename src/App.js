import React from 'react';
import { Provider } from 'react-redux';
import { store } from './redux';
import Content from './Content';

function App() {
  return (
    <Provider store={store}> 
      <Content />
    </Provider>
  );
}

export default App;