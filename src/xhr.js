
class Debouncer {
  timeout;

  debounce = function(fn, time, immediate) {
    return (...args) => {
      const later = () => {
        this.timeout = null;
        if (!immediate) fn.apply(this, args);
      };
      const callNow = immediate && !this.timeout;
      clearTimeout(this.timeout);
      this.timeout = setTimeout(later, time);
      if (callNow) fn.apply(this, args);
    };
  };

  cancel = function () {
    clearTimeout(this.timeout);
  }
}

const debouncer = new Debouncer();

class XHR {
  constructor() {
    this.request = new XMLHttpRequest();

    this.request.onerror = function() {
      alert("Request failed");
    };
  }
  
  makeRequest = debouncer.debounce(async (searchString, cb) => {
    if (this.request && this.request.readyState !== 4) {
      this.request.abort();
    }
    this.request.open('GET', `https://api.savetime.net/v1/client/suggest/item?q=${searchString}`);
    this.request.send();
    this.request.onload = () => {
      if (this.request.status !== 200) {
        console.log(`Error ${this.request.status}: ${this.request.statusText}`);
      } else {
        cb(JSON.parse(this.request.response));
      }
    };
  }, 500, false);

  getSuggestions = (searchString, cb) => {
    this.makeRequest(searchString, cb);
  }
}

const xhr = new XHR();

export default xhr;