import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getSuggestions } from './redux';

import Input from './Input';

const Content = () =>  {
  const [searchString, setSearchString] = useState('');
  const dispatch = useDispatch();
  const sug = useSelector((state => state.result));

  const names = sug?.map(item =>  item.name) || [];

  useEffect(() => {
    dispatch(getSuggestions(searchString));
  }, [searchString, dispatch]);

  const handleChange = (e) => {
    setSearchString(e);
  }

  return (
    <div
      style={{
        width: '400px',
        padding: '100px 20px'
      }}
    >
      <Input
        value={searchString}
        onChange={handleChange}
        items={names}
      />
    </div>
  );
}

export default Content;